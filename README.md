# PTAL Official Registry

Registry providing the different components of [PTAL](https://gitlab.lisn.upsaclay.fr/PTAL).

## Installation

To add this registry to your Julia installation type `]` to enter the
package mode of the REPL and then type:

```
pkg> registry add "https://gitlab.lisn.upsaclay.fr/PTAL/Registry"
```

## Register a new package

> :warning: This action is reserved to PTAL maintainers

Set up a CI/CD pipeline as described in [PTAL/Documentation](https://gitlab.lisn.upsaclay.fr/PTAL/Documentation). Then, new package and subsequent new versions will be registered as soon as a new version tag is pushed (on main branch). 
